#include <iostream>
#include <string>

using namespace std;

int main()
{
	string str1 = "hello";
	string str2 = str1+" world";
	
	cout<<"str1 = "<<str1<<endl;
	cout<<"the 3rd character is "<<str1[2]<<endl;
	cout<<"str2 = "<<str2<<endl;
}
